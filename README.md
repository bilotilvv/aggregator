### "Message Aggregator" overview

* Process "Aggregate inbound messages" (consumer)
* Process "Close aggregates by specific criteria" (cronjob)

### How to run app 

```
# Run RabbiMQ
docker run -p 15672:15672 -p 5672:5672 rabbitmq:3.8.9-management

# Run Redis
docker run -p 6379:6379 redis:2.8.23

# Build consumer
go build cmd/consumer_event_aggregator.go

# Build cronjob
go build cmd/command_close_aggregates.go

# Run consumer
./consumer_event_aggregator

# Run cronjob
./command_close_aggregates
```

Publish message into inbound queue: http://localhost:15672/#/queues/%2F/hello-events

Inbound Message structure

```
{
  "id": "<string>",
  "body": "<string>"
}
```