package app

import (
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
)

type EventId struct {
	key string
}

func NewEventId(key string) aggregate.EventId {
	return EventId{key: key}
}

func (i EventId) ToString() string {
	return i.key
}

func (i EventId) Equals(id2 aggregate.EventId) bool {
	return i.ToString() == id2.ToString()
}
