package aggregate

import (
	"fmt"
	"time"
)

type Id struct {
	Key string
}

type ClosingReason struct {
	Name string
}

type ClosingCriteria struct {
	MaxNumberOfEvents     int64
	TtlOfAggregateSeconds int64
}

type Aggregate struct {
	Id             Id
	CreatedAt      int64
	NumberOfEvents int64
}

func NewAggregate(id Id) *Aggregate {
	return &Aggregate{Id: id, CreatedAt: time.Now().Unix(), NumberOfEvents: 0}
}

func (a *Aggregate) Incr() {
	a.NumberOfEvents++
}

func (a *Aggregate) GetClosingReasons(cc ClosingCriteria) []ClosingReason {
	var reasons []ClosingReason

	if a.NumberOfEvents >= cc.MaxNumberOfEvents {
		reasons = append(
			reasons,
			ClosingReason{
				Name: fmt.Sprintf(
					"Reached max number of events. Occurences: %d. Max allowed: %d",
					a.NumberOfEvents,
					cc.MaxNumberOfEvents,
				),
			},
		)
	}

	ageSec := time.Now().Unix() - a.CreatedAt

	if ageSec >= cc.TtlOfAggregateSeconds {
		reasons = append(
			reasons,
			ClosingReason{
				Name: fmt.Sprintf("Reached TTL. Age: %d sec. TTL: %d sec", ageSec, cc.TtlOfAggregateSeconds),
			},
		)
	}

	return reasons
}
