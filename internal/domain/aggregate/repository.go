package aggregate

type Repository interface {
	Save(a *Aggregate) error
	FindById(id *Id) (*Aggregate, error)
	FindToClose(criteria ClosingCriteria) ([]Id, error)
	Remove(id *Id) error
}
