package aggregate

import "log"

type Service interface {
	Aggregate(e Event) error
	FindToClose(criteria ClosingCriteria) ([]Id, error)
	CloseAggregates(ids []Id, criteria ClosingCriteria) error
}

type AggregatorService struct {
	repository Repository
	producer   Producer
}

func NewAggregatorService(repository Repository, producer Producer) *AggregatorService {
	return &AggregatorService{repository: repository, producer: producer}
}

func (s AggregatorService) Aggregate(e Event) error {
	log.Printf("Aggregte event ID '%s'", e.Id.ToString())

	var id = &Id{Key: e.Id.ToString()}

	log.Printf("Process aggregate IdKey '%s'", id.Key)

	var a, err = s.repository.FindById(id)
	if err != nil {
		a = NewAggregate(*id)
	}

	a.Incr()

	_ = s.repository.Save(a)

	return nil
}

func (s AggregatorService) FindToClose(criteria ClosingCriteria) ([]Id, error) {
	return s.repository.FindToClose(criteria)
}

func (s AggregatorService) CloseAggregates(ids []Id, criteria ClosingCriteria) error {
	for _, id := range ids {
		log.Printf("Close aggregate. IdKey '%s'", id.Key)
		a, _ := s.repository.FindById(&id)
		reasons := a.GetClosingReasons(criteria)

		if len(reasons) == 0 {
			log.Println("No needs to close aggregate", a)

			continue
		}

		log.Println("Closing reasons ", reasons)

		_ = s.producer.Publish(AggregatedMessage{Body: id.Key})
		_ = s.repository.Remove(&id)
	}

	return nil
}
