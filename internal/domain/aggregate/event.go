package aggregate

type EventId interface {
	ToString() string
	Equals(id2 EventId) bool
}

type Event struct {
	Id EventId
}
