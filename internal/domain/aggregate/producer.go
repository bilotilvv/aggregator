package aggregate

type AggregatedMessage struct {
	Body string
}

type Producer interface {
	Publish(m AggregatedMessage) error
}
