package redis

import (
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"log"
	"strconv"
	"time"
)

const (
	storageByCreationTimeKey   = "by_creation_time"
	storageByNumberOfEventsKey = "by_number_of_events"
)

type AggregateRepository struct {
	client *redis.Client
}

func NewRepository(redisClient *redis.Client) aggregate.Repository {
	return &AggregateRepository{client: redisClient}
}

func (r *AggregateRepository) Save(m *aggregate.Aggregate) error {
	ctx := context.Background()

	r.client.ZAdd(ctx, storageByCreationTimeKey, &redis.Z{Score: float64(m.CreatedAt), Member: m.Id.Key})
	r.client.ZAdd(ctx, storageByNumberOfEventsKey, &redis.Z{Score: float64(m.NumberOfEvents), Member: m.Id.Key})

	log.Printf(
		"Aggregate was saved: {IdKey: %s, NumberOfEvents: %d, CreatedAt: %s}",
		m.Id.Key,
		m.NumberOfEvents,
		time.Unix(m.CreatedAt, 0).Format(time.RFC3339),
	)

	return nil
}

func (r *AggregateRepository) FindById(id *aggregate.Id) (*aggregate.Aggregate, error) {
	var a *aggregate.Aggregate

	ctx := context.Background()

	createdAt, err := r.client.ZScore(ctx, storageByCreationTimeKey, id.Key).Result()

	if err != nil {
		return nil, fmt.Errorf("aggregate with ID %s not found", id.Key)
	}

	numberOfEvents, err := r.client.ZScore(ctx, storageByNumberOfEventsKey, id.Key).Result()

	if err != nil {
		return nil, fmt.Errorf("aggregate with ID %s not found", id.Key)
	}

	a = &aggregate.Aggregate{
		Id:             *id,
		CreatedAt:      int64(createdAt),
		NumberOfEvents: int64(numberOfEvents),
	}

	return a, nil
}

func (r *AggregateRepository) FindToClose(criteria aggregate.ClosingCriteria) ([]aggregate.Id, error) {
	var ids []aggregate.Id
	var idsMap map[string]string
	idsMap = make(map[string]string)

	ctx := context.Background()

	maxCreatedAt := time.Now().Unix() - criteria.TtlOfAggregateSeconds
	maxNumberOfEvents := criteria.MaxNumberOfEvents

	idsString, _ := r.client.ZRangeByScore(
		ctx,
		storageByCreationTimeKey,
		&redis.ZRangeBy{Min: "0", Max: strconv.FormatInt(maxCreatedAt, 10)},
	).Result()

	for _, id := range idsString {
		idsMap[id] = id
	}

	idsString, _ = r.client.ZRangeByScore(
		ctx,
		storageByNumberOfEventsKey,
		&redis.ZRangeBy{Min: strconv.FormatInt(maxNumberOfEvents, 10), Max: "+inf"},
	).Result()

	for _, id := range idsString {
		idsMap[id] = id
	}

	for _, idString := range idsMap {
		ids = append(ids, aggregate.Id{Key: idString})
	}

	return ids, nil
}

func (r *AggregateRepository) Remove(id *aggregate.Id) error {
	ctx := context.Background()

	r.client.ZRem(ctx, storageByCreationTimeKey, id.Key)
	r.client.ZRem(ctx, storageByNumberOfEventsKey, id.Key)

	return nil
}
