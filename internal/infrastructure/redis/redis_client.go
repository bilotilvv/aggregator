package redis

import (
	"context"
	"github.com/go-redis/redis/v8"
	"log"
)

func NewRedisClient(addr string, password string) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password, // no password set
		DB:       0,        // use default DB
	})
	ctx := context.Background()

	pong, err := client.Ping(ctx).Result()

	log.Println(pong, err)

	failOnError(err, "Cannot connect to Redis")

	return client
}
