package nullproducer

import (
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
	"log"
)

type Producer struct {
}

func (p Producer) Publish(m aggregate.AggregatedMessage) error {
	log.Println("Msg: ", m.Body)

	return nil
}
