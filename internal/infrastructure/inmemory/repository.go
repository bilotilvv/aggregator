package inmemory

import (
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
	"fmt"
	"log"
)

type Repository struct {
	maps map[string]*aggregate.Aggregate
}

func NewRepository() aggregate.Repository {
	return &Repository{maps: make(map[string]*aggregate.Aggregate)}
}

func (r *Repository) Save(m *aggregate.Aggregate) error {
	r.maps[m.Id.Key] = m

	log.Println("Save aggregate", m)

	return nil
}

func (r *Repository) FindById(id *aggregate.Id) (*aggregate.Aggregate, error) {
	a, ok := r.maps[id.Key]
	if true != ok {
		return nil, fmt.Errorf("aggregate with ID %s not found", id.Key)
	}

	return a, nil
}

func (r *Repository) FindToClose(criteria aggregate.ClosingCriteria) ([]aggregate.Id, error) {
	var ids []aggregate.Id

	for _, a := range r.maps {
		if len(a.GetClosingReasons(criteria)) > 0 {
			ids = append(ids, a.Id)
		}
	}

	return ids, nil
}

func (r *Repository) Remove(id *aggregate.Id) error {
	delete(r.maps, id.Key)

	return nil
}
