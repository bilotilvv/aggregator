package rabbitmq

import (
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
	"encoding/json"
	"log"

	"github.com/streadway/amqp"
)

type Msg struct {
	Body string `json:"body"`
}

type Producer struct {
	conn *amqp.Connection
	routingKey string
}

func NewProducer(conn *amqp.Connection, routingKey string) aggregate.Producer {
	return &Producer{
		conn,
		routingKey,
	}
}

func (p *Producer) Publish(msg aggregate.AggregatedMessage) error {
	ch, err := p.conn.Channel()
	failOnError(err, "Failed to open a channel")

	body, _ := json.Marshal(Msg{Body: msg.Body})
	err = ch.Publish(
		"",                 // exchange
		p.routingKey, // routing key
		false,              // mandatory
		false,              // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		},
	)
	log.Printf(" [x] Sent %s", body)
	failOnError(err, "Failed to publish a message")

	return nil
}
