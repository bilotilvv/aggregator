package main

import (
	"bitbucket.org/bilotilvv/aggregator/cmd/helper"
	"bitbucket.org/bilotilvv/aggregator/cmd/service_locator"
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
)

// init is invoked before main()
func init() {
	helper.InitDotEnv()
}

func main() {
	var cc = aggregate.ClosingCriteria{
		MaxNumberOfEvents:     2,
		TtlOfAggregateSeconds: 4,
	}

	s := service_locator.NewAggregatorServiceFromEnv()

	ids, err := s.FindToClose(cc)
	helper.FailOnError(err, "Error occurred while searching for aggregates to close")

	err = s.CloseAggregates(ids, cc)
	helper.FailOnError(err, "Error occurred while closing aggregates")
}
