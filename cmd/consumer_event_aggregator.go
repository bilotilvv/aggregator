package main

import (
	"bitbucket.org/bilotilvv/aggregator/cmd/helper"
	"bitbucket.org/bilotilvv/aggregator/cmd/service_locator"
	"bitbucket.org/bilotilvv/aggregator/internal/app"
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
	"bitbucket.org/bilotilvv/aggregator/internal/infrastructure/rabbitmq"
	"encoding/json"
	"log"
	"os"
)

type InboundMessage struct {
	Id string `json:"id"`
	Body string `json:"body"`
}

// init is invoked before main()
func init() {
	helper.InitDotEnv()
}

func main() {
	rabbitmqUrl := os.Getenv("RABBITMQ_URL")
	rabbitmqInboundQueue := os.Getenv("RABBITMQ_INBOUND_QUEUE")

	rabbitmqConnection := rabbitmq.NewConnection(rabbitmqUrl, rabbitmqInboundQueue)
	defer rabbitmqConnection.Close()

	ch, err := rabbitmqConnection.Channel()
	helper.FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	msgs, err := ch.Consume(
		rabbitmqInboundQueue, // queue
		"",     // consumer
		true,    // auto-ack
		false,  // exclusive
		false,   // no-local
		false,   // no-wait
		nil,       // args
	)
	helper.FailOnError(err, "Failed to register a consumer")
	s := service_locator.NewAggregatorServiceFromEnv()

	forever := make(chan bool)
	go func() {
		for d := range msgs {
			log.Printf("Received a message raw: %s", d.Body)

			var msg InboundMessage
			err := json.Unmarshal(d.Body, &msg)
			if err != nil {
				log.Println(err)
				continue
			}

			if msg.Id == "" {
				log.Printf("AggregatedMessage must have Id: {Id: %s, Body: %s}", msg.Id, msg.Body)
				continue
			}

			log.Printf("Unmarshal a message {Id: %s, Body: %s}", msg.Id, msg.Body)
			_ = s.Aggregate(aggregate.Event{Id: app.NewEventId( msg.Id)})
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}