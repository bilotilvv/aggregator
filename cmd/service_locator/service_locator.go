package service_locator

import (
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
	"bitbucket.org/bilotilvv/aggregator/internal/infrastructure/rabbitmq"
	"bitbucket.org/bilotilvv/aggregator/internal/infrastructure/redis"
	"log"
	"os"
)

type RedisOptions struct {
	addr string
}

type RabbitMqOptions struct {
	url        string
	routingKey string
	queueName  string
}

func NewAggregatorService(redisOptions RedisOptions, rabbitMqOptions RabbitMqOptions) aggregate.Service {
	return aggregate.NewAggregatorService(
		redis.NewRepository(
			redis.NewRedisClient(redisOptions.addr, ""),
		),
		rabbitmq.NewProducer(
			rabbitmq.NewConnection(
				rabbitMqOptions.url,
				rabbitMqOptions.queueName,
			),
			rabbitMqOptions.routingKey,
		),
	)
}

func NewAggregatorServiceFromEnv() aggregate.Service {
	redisUrl := lookupEnvOrFail("REDIS_URL")
	rabbitmqUrl := lookupEnvOrFail("RABBITMQ_URL")
	rabbitmqQueue := lookupEnvOrFail("RABBITMQ_OUTBOUND_QUEUE")

	return NewAggregatorService(
		RedisOptions{
			addr: redisUrl,
		},
		RabbitMqOptions{
			rabbitmqUrl,
			rabbitmqQueue,
			rabbitmqQueue,
		},
	)
}

func lookupEnvOrFail(envName string) string {
	envValue, exists := os.LookupEnv(envName)

	if !exists {
		log.Fatalf("Env '%s' not found", envName)
	}

	return envValue
}
