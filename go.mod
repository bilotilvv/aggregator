module bitbucket.org/bilotilvv/aggregator

go 1.15

require (
	github.com/go-redis/redis/v8 v8.3.0
	github.com/joho/godotenv v1.3.0
	github.com/streadway/amqp v1.0.0
)
