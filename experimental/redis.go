package main

import (
    "context"
    "fmt"
)

import "github.com/go-redis/redis/v8"

func main() {
    client := redis.NewClient(&redis.Options{
        Addr:     "localhost:6379",
        Password: "", // no password set
        DB:       0,  // use default DB
    })

    ctx := context.Background()

    pong, err := client.Ping(ctx).Result()

    fmt.Println(pong, err)

    // Hashes
    client.HSet(ctx, "hash", "key", "value")
    var val2 string
    val2, err = client.HGet(ctx, "hash", "key").Result()
    fmt.Println(val2, err)

    var val3 string
    client.HDel(ctx, "hash", "key")
    val3, err = client.HGet(ctx, "hash", "key").Result()

    fmt.Println(val3)
    fmt.Println(err)

    // Sorted Set
    var zkey string
    var val4 float64
    client.ZAdd(ctx, zkey, &redis.Z{Score: 1111, Member: "vid_id_1"})
    client.ZAdd(ctx, zkey, &redis.Z{Score: 2222, Member: "vid_id_2"})
    client.ZAdd(ctx, zkey, &redis.Z{Score: 3333, Member: "vid_id_3"})

    client.ZRem(ctx, zkey, "vid_id_2")
    val4, err = client.ZIncrBy(ctx, zkey, 1, "vid_id_3").Result()

    fmt.Println(val4)
    fmt.Println(err)

    val4, err = client.ZScore(ctx, zkey, "vid_id_3").Result()

    fmt.Println(val4)
    fmt.Println(err)

    var val5 []string
    val5, err = client.ZRangeByScore(ctx, zkey, &redis.ZRangeBy{Min: "0", Max: "3333"}).Result()

    fmt.Println(val5)
    fmt.Println(err)
}

