package main

import (
	"bitbucket.org/bilotilvv/aggregator/internal/app"
	"bitbucket.org/bilotilvv/aggregator/internal/domain/aggregate"
	"bitbucket.org/bilotilvv/aggregator/internal/infrastructure/rabbitmq"
	"bitbucket.org/bilotilvv/aggregator/internal/infrastructure/redis"
)

func NewAggregatorService() aggregate.Service {
	return aggregate.NewAggregatorService(
		redis.NewRepository(
			redis.NewRedisClient("localhost:6379", ""),
		),
		rabbitmq.NewProducer(
			rabbitmq.NewConnection(
				"amqp://guest:guest@localhost:5672/",
				"hello-aggregated",
			),
			"hello-aggregated",
		),
	)
	//return aggregate.NewAggregatorService(inmemory.NewRepository(), nullproducer.Producer{})
}

func main() {
	s := NewAggregatorService()
	_ = s.Aggregate(aggregate.Event{Id: app.NewEventId("Test Key 1")})
	_ = s.Aggregate(aggregate.Event{Id: app.NewEventId("Test Key 1")})
	_ = s.Aggregate(aggregate.Event{Id: app.NewEventId("Test Key 1")})
}
